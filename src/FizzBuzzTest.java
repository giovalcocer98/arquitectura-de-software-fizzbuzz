
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FizzBuzzTest {

	FizzBuzz fizzBuzz = new FizzBuzz();
	
	@Test
	public void devuelve2SigeneroFizzBuzzPara2() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(2),"2");
	}
	
	@Test
	public void devuelve4SigeneroFizzBuzzPara4() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(4),"4");
	}
	
	@Test
	public void devuelveFizzSigeneroFizzBuzzPara3() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(3),"Fizz");
	}
	
	@Test
	public void devuelveFizzSigeneroFizzBuzzPara6() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(6),"Fizz");
	}
	
	@Test
	public void devuelveBuzzSigeneroFizzBuzzPara5() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(5),"Buzz");
	}
	
	@Test
	public void devuelveBuzzSigeneroFizzBuzzPara10() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(10),"Buzz");
	}
	
	@Test
	public void devuelveFizzBuzzSigeneroFizzBuzzPara0() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(0),"Fizz Buzz");
	}
	
	@Test
	public void devuelveFizzBuzzSigeneroFizzBuzzPara15() {
		assertEquals(fizzBuzz.generarFizzBuzzParaUnNumero(15),"Fizz Buzz");
	}



}
