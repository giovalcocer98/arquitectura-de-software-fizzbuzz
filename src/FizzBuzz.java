

public class FizzBuzz {
	
	public String generarFizzBuzzParaUnNumero(int numero)
	{
		if (numero % 3 == 0 && numero % 5 == 0)
		{
			return "Fizz Buzz";                    
		}
		else 
			if(numero % 3 == 0)
			{
				return "Fizz";
			}
			else 
				if(numero % 5 == 0)
				{
					return "Buzz";
				}
				else
					return Integer.toString(numero);
	}
}
